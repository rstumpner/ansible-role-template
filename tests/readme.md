## Notice
This is the deprecated folder for Native Testing of the Ansible Role . To enable the Testing with the deprecated method set the CI/CD environment variable `CICD_ANSIBLE_NATIVE: "true"`

- default is CICD_ANSIBLE_NATIVE: "false"