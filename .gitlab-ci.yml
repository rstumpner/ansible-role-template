# .gitlab-ci.yml
stages:
  - validation
  - build
  - static-tests
  - package
  - prepare
  - deploy
  - verify
  
variables:
  LC_ALL: 'C.UTF-8'
  LANG: 'C.UTF-8'
  GIT_SSL_NO_VERIFY: "1"
  ANSIBLE_FORCE_COLOR: 'true'
  ANSIBLE_HOST_KEY_CHECKING: 'false'
  ANSIBLE_STDOUT_CALLBACK: 'junit'
  JUNIT_OUTPUT_DIR: 'ansible-junit-results' 
  JUNIT_FAIL_ON_CHANGE: 'true' 
  JUNIT_HIDE_TASK_ARGUMENTS: 'true'

  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
#  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_TLS_CERTDIR: ''
  STAGE_PACKAGE_DOCS_DIR: "docs"
  STAGE_PACKAGE_DOCS_BUILD: "build"
## disable Molecule default enabled
#  MOLECULE_DISABLED: "true"
  CICD_MOLECULE: "true"
  CICD_MOLECULE_SCENARIO: "native"
# Ansible CI/CD Pipline default variables
  CICD_ANSIBLE_NATIVE: "true"
  ANSIBLE_DEFAULT_INVENTORY: "tests/ansible-docker/hosts"
  ANSIBLE_DEFAULT_PLAYBOOK: "tests/test.yml"
  ANSIBLE_DEFAULT_TAGS: ""
# Ansible CI/CD Pipleline variables for Stage validation
# Ansible CI/CD Pipleline variables for Stage build
  ANSIBLE_BUILD_TAGS: "build"
  ANSIBLE_TAG_STATICTEST: "static_test"
  ANSIBLE_TAG_PACKAGE: "package"
  ANSIBLE_PREPARE_TAGS: "prepare"
  ANSIBLE_DEPLOY_TAGS: "deploy"
  ANSIBLE_VERIFY_TAGS: "verify"



services:
  - docker:dind

image: ubuntu:18.04


before_script:
  - apt-get update -qq
  - apt install --no-install-recommends -y python3 python3-dev software-properties-common libapt-pkg-dev git curl vim iputils-ping mtr dnsutils rsync tree python3-pip python3-apt python3-docker python-pip build-essential python-dev
  - apt-add-repository ppa:ansible/ansible
  - curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/trusted.gpg.d/docker-ce.asc
  - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
  - apt update
  - apt install -y ansible python3-markupsafe python3-ecdsa libyaml-0-2 python3-jinja2 python3-yaml python3-junit.xml python3-paramiko python3-httplib2 python-crypto sshpass docker-ce
  - python -V
  - pip install virtualenv==20.6.0
  - virtualenv --python 3 --system-site-packages virtenv
  - source virtenv/bin/activate
  - pip3 install setuptools ansible==2.9.23 molecule molecule-docker docker apt-wrapper
  - ansible --version
  - docker --version
  - molecule --version
  - docker info


.install_os_packages:
  before_script: &install_os_packages_install
    - apt-get update -qq
    - apt install --no-install-recommends -y python3 python3-dev python3-setuptools software-properties-common libapt-pkg-dev git curl vim iputils-ping mtr dnsutils rsync tree python3-pip python3-apt python3-docker python-pip build-essential python-dev python3-markupsafe python3-ecdsa libyaml-0-2 python3-jinja2 python3-yaml python3-paramiko python3-junit.xml python3-httplib2 python-crypto sshpass


.install_ansible_packages:
  before_script: &install_ansible_packages
    - apt-add-repository ppa:ansible/ansible
    - apt update
    - apt install -y ansible 

.install_ansible_pip:
  before_script:
    - pip3 install ansible==2.9.23
    - ansible --version

.install_docker_packages:
  before_script: &install_docker_packages
    - curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/trusted.gpg.d/docker-ce.asc
    - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
    - apt update
    - apt install -y docker-ce

.install_python_virtenv:
  before_script: 
  - pip install virtualenv==20.6.0
  - virtualenv --python 3 --system-site-packages virtenv
  - source virtenv/bin/activate


.install_docker_pip:
  before_script:
    - pip3 install docker

.install_molecule_pip:
  before_script:
    - pip3 install setuptools docker apt-wrapper molecule==3.0.5 molecule-docker


.verify_environment_install:
  before_script: &verify_environment_install
    - ansible --version
    - docker --version
    - molecule --version
    - docker info

.test_static_pytest_install:
  before_script: &test_static_install
  - apt-get update -qq
  - apt install --no-install-recommends -y python3 python3-dev python3-setuptools software-properties-common git curl vim iputils-ping mtr dnsutils rsync tree python3-pip python3-docker build-essential  jq sshpass python3-markupsafe python3-ecdsa libyaml-0-2 python3-jinja2 python3-yaml python3-paramiko python3-httplib2 python-crypto sshpass
  - pip3 install virtualenv==20.6.0 pytest netaddr ciscoconfparse==1.5.36
#  - virtualenv virtenv
#  - source virtenv/bin/activate
  - python -V
#  - pip install pytest json netaddr ciscoconfparse==1.5.36
  - echo $ANSIBLE_VAULT_KEY > /tmp/vault-password

.deployment_sidecar:
  before_script: 
    - !reference [".install_os_packages","before_script"]
    - !reference [".install_ansible_packages","before_script"]
    - !reference [".install_docker_packages","before_script"]
    - !reference [".install_python_virtenv","before_script"]
    - !reference [".install_ansible_pip","before_script"]
    - !reference [".install_molecule_pip","before_script"]
    - !reference [".verify_environment_install","before_script"]
#  before_script: !reference [".install_os_packages",".install_ansible_packages",".install_docker_packages","before_script"]
#  extend:
#    - .install_os_packages
#    - .install_ansible_packages
#    - .install_docker_packages
#    - .verify_environment_install



validation-molecule-install:
  stage: validation
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_MOLECULE == "true"'
  script:
    - 'molecule list'
    - 'molecule matrix test'

validation-molecule-role:
  stage: validation
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_MOLECULE == "true"'
  script:
    - 'molecule lint'

validation-native-role-deploymentsidecar:
  stage: validation
  before_script: 
    - !reference [".deployment_sidecar","before_script"]
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  script:
    - 'ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY} ${ANSIBLE_DEFAULT_PLAYBOOK} --syntax-check'



validation-native-role-beforescript:
  stage: validation
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  script:
    - 'ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY} ${ANSIBLE_DEFAULT_PLAYBOOK} --syntax-check'

build-native:
  stage: build
  artifacts:
    when: always
    paths:
      - tests/
      - ansible-junit-results/
    reports:
      junit: 
        - ansible-junit-results/*.xml
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  script:
    - ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY} ${ANSIBLE_DEFAULT_PLAYBOOK} --tags ${ANSIBLE_BUILD_TAGS}

test_static_ansible-native:
  stage: build
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  script:
    - ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY} ${ANSIBLE_DEFAULT_PLAYBOOK} --tags ${ANSIBLE_TAG_STATICTEST}


test_static_pytest-native:
   stage: static-tests
   artifacts:
    when: always
    reports:
      junit: report.xml
   rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
   before_script: !reference [.test_static_pytest_install, before_script]
   script:
     - cd tests/pytest
     - pytest --junitxml=../../report.xml

package-configs-native:
  stage: package
  variables:
    PACKAGENAME: '${CI_PROJECT_NAME}-config'
    VERSION: '1.0.0'
#   VERSION: ${CI_COMMIT_TAG}
  image: curlimages/curl:latest
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  before_script:
     - echo "override before script"
  script:
    - tar -czvf ${PACKAGENAME}.${VERSION}.tar.gz tests/${STAGE_PACKAGE_DOCS_BUILD}/
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${PACKAGENAME}.${VERSION}.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGENAME}/${VERSION}/${PACKAGENAME}.${VERSION}.tar.gz"'

package-documentation-native:
  stage: package
  variables:
    PACKAGENAME: '${CI_PROJECT_NAME}-doc'
    VERSION: '1.0.0'
#   VERSION: ${CI_COMMIT_TAG}
  image: curlimages/curl:latest
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  before_script:
     - echo "override before script"
  script:
    - tar -czvf ${PACKAGENAME}.${VERSION}.tar.gz tests/${STAGE_PACKAGE_DOCS_DIR}/
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${PACKAGENAME}.${VERSION}.tar.gz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGENAME}/${VERSION}/${PACKAGENAME}.${VERSION}.tar.gz"'

diff-mode-native:
  stage: prepare
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  script:
    - 'ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY} ${ANSIBLE_DEFAULT_PLAYBOOK} --skip-tags build --diff'

check-mode-native:
  stage: prepare
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  script:
    - 'ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY} ${ANSIBLE_DEFAULT_PLAYBOOK} --skip-tags build --check'

testing-with-molecule-check:
  stage: prepare
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_MOLECULE == "true"'
  script:
    - 'molecule check'

#testing-with-molecule:
#  stage: prepare
#  artifacts:
#    when: always
#    paths:
#      - tests/
#  rules:
#    - if: '$CICD_MOLECULE == "true"'
#  script:
#    - 'molecule test'

testing-with-molecule-scenario:
  stage: prepare
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_MOLECULE == "true"'
  script:
    - echo ${CICD_MOLECULE_SCENARIO}
    - 'molecule test -s ${CICD_MOLECULE_SCENARIO}'


prepare-test-native:
  stage: prepare
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  script:
    - 'ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY} ${ANSIBLE_DEFAULT_PLAYBOOK} --skip-tags build --tags ${ANSIBLE_PREPARE_TAGS}'

deploy-test-native:
  stage: deploy
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  script:
    - 'ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY} ${ANSIBLE_DEFAULT_PLAYBOOK} --skip-tags build --tags ${ANSIBLE_DEPLOY_TAGS}'

verify-native:
  stage: verify
  artifacts:
    when: always
    paths:
      - tests/
  rules:
    - if: '$CICD_ANSIBLE_NATIVE == "true"'
  script:
    - 'ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY} ${ANSIBLE_DEFAULT_PLAYBOOK} --skip-tags build --tags ${ANSIBLE_VERIFY_TAGS}'